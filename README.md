# vmm-docs

A place to keep info about vmm

# Baseline
About [baseline](https://gitlab.cern.ch/rowang/vmm-docs/-/wikis/Baseline).

# Difference between sTGC and MM
[Difference between sTGC and MM](https://gitlab.cern.ch/rowang/vmm-docs/-/wikis/diff-stgc-mm)
